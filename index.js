const path = require ('path');
const express = require('express');
const db = require('./queries');
const {logger, notFoundHandler, internalServerError} = require('./middleware');

const app = express();
const port = process.env.PORT || 5000;

//view engine
app.use(express.static('public'));
app.set('view engine', 'ejs');

// Body parser, reading data from body into req.body
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//login dashboard
app.get('/login', (req, res) => {
    res.render('login')
})
//login authentication
app.post('/login', (req, res) => {
    if (req.body.username === 'admin' && req.body.password === 'admin') {
        res.render('home')
    } else {
        res.status(401).send({'message': 'unauthorized'})
    }
})

app.get('/create', (req, res) =>{
    res.render('create');
})

// routing database
app.get('/players',db.getPlayers) // list all players
app.get('/players/:id',db.getPlayersById) // get single player based on id
app.post('/players',db.createPlayers) // create new player
app.put('/players/:id',db.updatePlayers) // update existing player data
app.delete('/players/:id',db.deletePlayers) // delete player data

//implementasi middleware 404 & logger
app.use(logger);
app.use(notFoundHandler);
app.use(internalServerError);

app.listen(port, () => {
    console.log(`Application is running on port ${port}`);
});

