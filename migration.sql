CREATE TABLE players (
  id SERIAL PRIMARY KEY,
  name VARCHAR(30),
  email VARCHAR(30),
  experience NUMERIC DEFAULT 0,
  lvl NUMERIC DEFAULT 0,
  created_at TIMESTAMP DEFAULT now()
);

INSERT INTO players (name, email, experience, lvl) VALUES
  ('sophie','skearsley0@ifeng.com',21,11),
  ('burl','bnansom1@amazon.co.jp',50,9),
  ('alene','adurrans2@prlog.org',16,2),
  ('edythe','ecolcomb3@aol.com',61,14),
  ('annmaria','akrzyzaniak4@merriam-webster.com',25,11);

CREATE TABLE players-bio (
  id SERIAL PRIMARY KEY,
  name VARCHAR(30),
  age NUMERIC,
  FOREIGN KEY (name) REFERENCES players (name)
);

INSERT INTO players-bio (age) VALUES
  (31),
  (20),
  (34),
  (27),
  (18);

CREATE TABLE players-history (
  id SERIAL PRIMARY KEY,
  name VARCHAR(30),
  lvl NUMERIC,
  last_login TIMESTAMP
  FOREIGN KEY (name) REFERENCES players (name)
);  