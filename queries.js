// connect to DB
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres', // your db user
  host: 'localhost',
  database: 'chapter6', // your db name
  password: '123456', // your db password
  port: 5432,
})

const getPlayers = (request, response) => {
    pool.query('SELECT * FROM players ORDER BY id ASC', (error, results) => {
      if (error) {
        response.status(500).json({
            status:'fail',
            errors:err.message
        })
      }
      response.status(200).json(results.rows)
      response.render('players/all', {data: results})
    })
}

const getPlayersById = (request, response) => {
    const id = parseInt(request.params.id)
  
    pool.query('SELECT * FROM players WHERE id = $1', [id], (error, results) => { // query: get player by id
      if (error) {
        response.status(500).json({
            status:'fail',
            errors:err.message
        })
      }
      response.status(200).json(results.rows)
    })
}

const createPlayers = (request, response) => {
    const { name, email } = request.body
  
    pool.query('INSERT INTO players (name, email) VALUES ($1, $2) RETURNING *', [name, email], (error, results) => { // set values for name and email
      if (error) {
        response.status(500).json({
            status:'fail',
            errors:err.message
        })
      } else if (!Array.isArray(results.rows) || results.rows.length < 1) { // if results has no rows or there is no result
        response.status(500).json({
            status:'fail',
            errors:err.message
        })
      }
      response.status(201).send(`Players added with ID: ${results.rows[0].id}`)
    })
}

const updatePlayers = (request, response) => {
    const id = parseInt(request.params.id)
    const { name, email } = request.body // get data from request body
  
    pool.query(
      'UPDATE players SET name = $1, email = $2 WHERE id = $3 RETURNING *',
      [name, email, id], // entries for query
      (error, results) => {
        if (error) {
            response.status(500).json({
                status:'fail',
                errors:err.message
            })
            // send response with status 500 or internal server error
        }
        if (typeof results.rows == 'undefined') {
            response.status(404).send(`Resource not found`);
        } else if (Array.isArray(results.rows) && results.rows.length < 1) {
            response.status(404).send(`Player not found`); // send 404 status with information 'Player not found'
        } else {
               response.status(200).send(`Players modified with ID: ${results.rows[0].id}`)
        }
      }
    )
}

const deletePlayers = (request, response) => {
    const id = parseInt(request.params.id) // get data from request parameter (id)
  
    pool.query('DELETE FROM players WHERE id = $1', [id], (error, results) => {
      if (error) {
        response.status(500).json({
            status:'fail',
            errors:err.message
        }) // send json with status 500
      }
      response.status(200).send(`Players deleted with ID: ${id}`)
    })
}

module.exports = {
    getPlayers,
    getPlayersById,
    createPlayers,
    updatePlayers,
    deletePlayers,
}